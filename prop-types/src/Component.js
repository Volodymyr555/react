import PropTypes from "prop-types";

function Component({name, age}) {
    return `After 5 years ${name} will be ${age + 5}`;
}

Component.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
}

export default Component;