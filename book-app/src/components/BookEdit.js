import React from "react";
import { useState } from "react";
import useBooksContext from "../hook/use-books-content";


function BookEdit({book, onSubmit}) {
    const [title, setTitle] = useState(book.title);
    const {editBookById} = useBooksContext();

    const handeChange =(event) =>{
        setTitle(event.target.value)
    }

    const hadleSubmit = (event) => {
        event.preventDefault()        
        onSubmit();
        editBookById(book.id, title);
    }
    return (
        <form className="book-edit" onSubmit={hadleSubmit}>
            <label>Title</label>
            <input className="input" value={title} onChange={handeChange}/>
            <button className="button is-primary">Save</button>
        </form>
    )
    
}

export default BookEdit