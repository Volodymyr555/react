import React from 'react';
import {useFormik} from "formik";
import * as Yup from "yup";

const initialValues = {
    name: "",
    email: "",
    chanel: "",
};

const onSubmit = (values) => {
    
};

const validate = (values) => {
    let errors ={}
    if (!values.name) {
       errors.name = 'Required' 
    }
    if(!values.email) {
        errors.email = 'Required'
    }
    else if (!/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i.test(values.email)) {
        errors.email = "Invalid email format";
    }
    if(!values.chanel) {
        errors.chanel = 'Required'
    }
    return errors
}

const validationSchema = Yup.object({
    name: Yup.string().required('Required!'),
    email: Yup.string().email('invalid email format').required('Required!'),
    chanel: Yup.string().required('Required!'),
});

function OldYouTubeForm(props) {

   const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    // validate,
})
   console.log(formik.touched);
    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                
                <div className='form-control'>
                <label htmlFor='name'>Name</label>
                <input 
                type='text'
                 id="name"name="name" 
                 onChange={formik.handleChange}
                 onBlur={formik.handleBlur}
                value={formik.values.name}
                />                
                {formik.errors.name ? <div className='error'>{formik.touched.name && formik.errors.name}</div> : null}
                </div>

                <div className='form-control'> 
                <label htmlFor='email'>E-mail</label>
                <input
                type='email' 
                id="email" 
                name="email" 
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}/>
                {formik.errors.email ? <div className='error'>{formik.touched.email && formik.errors.email}</div> : null}
                </div>

                <div className='form-control'>
                <label htmlFor='chanel'>Chanel</label>
                <input 
                type='text' 
                id="chanel" 
                name="chanel" 
                onChange={formik.handleChange}               
                onBlur={formik.handleBlur}
                value={formik.values.chanel}/>
                {formik.errors.chanel ? <div className='error'>{formik.touched.chanel && formik.errors.chanel}</div> : null}
                </div>

                <button type='submit'>Sumit</button>
            </form>
            
        </div>
    );
}

export default OldYouTubeForm;