import ReactDOM from 'react-dom/client';
import FavoritesCard from "../components/FavoritesCard"
import { screen } from "@testing-library/react";
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import {act} from 'react-dom/test-utils'

global.IS_REACT_ACT_ENVIRONMENT = true;
const initialState = {};
let container = null;
let root = null;
const mockStore = configureStore();
const store = mockStore(initialState);


beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

test("Button Component test", () => {

    const image = "React";
    const title = "React";
    const price = "React";
    const article = "React";
    const color = "React";
    const id = "React";
    const isFavorite = "React";
        

    act(() => {root.render(<Provider store={store}>
            <FavoritesCard image={image} title={title} price={price} article={article} color={color} id={id} isFavorite={isFavorite} />
        </Provider>);
    });

    const element = screen.getByRole('btnFavorite');
    expect(element).not.toBeUndefined();


    expect(element).toBeTruthy();

    
    expect(element.textContent).toBe(`Add to cart`);

    
});