import ReactDOM from 'react-dom/client';
import ProfileCard from "../components/ProfileCard"
import { screen } from "@testing-library/react";
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import {act} from 'react-dom/test-utils'

global.IS_REACT_ACT_ENVIRONMENT = true;
const initialState = {};
let container = null;
let root = null;
const mockStore = configureStore();
const store = mockStore(initialState);


beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

test("Button Component test", () => {

    const image = "React";
    const title = "React";
    const price = "React";
    const article = "React";
    const color = "React";
    const id = "React";
    const isFavorite = "React";
        

    act(() => {root.render(<Provider store={store}>
            <ProfileCard image={image} title={title} price={price} article={article} color={color} id={id} isFavorite={isFavorite} />
        </Provider>);
    });

    const element = screen.getByRole('btnProfile');
    console.log(element);
    expect(element).not.toBeUndefined();


    expect(element).toBeTruthy();

    
    expect(element.textContent).toBe(`Add to cart`);

    expect(element.outerHTML).toMatchSnapshot();
});