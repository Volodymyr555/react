import ReactDOM from 'react-dom/client';
import CartCard from "../components/CartCard"
import { screen } from "@testing-library/react";
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import {act} from 'react-dom/test-utils'

global.IS_REACT_ACT_ENVIRONMENT = true;
const initialState = {};
let container = null;
let root = null;
const mockStore = configureStore();
const store = mockStore(initialState);


beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

test("Button Component test", () => {

    const image = "React";
    const title = "React";
    const price = "React";
    const article = "React";
    const color = "React";
    const id = "React";
    const counter = "React";
        

    act(() => {root.render(<Provider store={store}>
            <CartCard image={image} title={title} price={price} article={article} color={color} id={id} counter={counter} />
        </Provider>);
    });

    const elementCrease = screen.findByRole('Crease');
    const elementDecrease = screen.findByRole('Decrease');
    const elementDelete = screen.queryByRole('DeleteCart');


    
    expect(elementCrease).not.toBeUndefined();
    expect(elementDecrease).not.toBeUndefined();
    expect(elementDelete).not.toBeUndefined();
    

    expect(elementCrease).toBeTruthy();
    expect(elementDecrease).toBeTruthy();
    expect(elementDelete).toBeTruthy();
    

    
    elementCrease.then(elementCrease=>expect(elementCrease.textContent).toBe('+')) ;
    elementDecrease.then(elementDecrease=>expect(elementDecrease.textContent).toBe('-'));
    expect(elementDelete.textContent).toBe('')   

    
});