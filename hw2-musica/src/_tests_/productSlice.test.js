import productSlice, {initialState} from "../components/store/reducers/productSlice";

describe('Reducer "productSlice" works', () => {

    test('Should return the init state', () => {
        const productSliceInit = productSlice(initialState, { type: "unknown" });
        expect(productSliceInit).toBe(initialState)});


})