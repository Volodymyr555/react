import ReactDOM from 'react-dom/client';
import CreateModal from "../components/CreateModal"
import { screen } from "@testing-library/react";
import {act} from 'react-dom/test-utils'
import * as reduxHooks from 'react-redux';

global.IS_REACT_ACT_ENVIRONMENT = true;
let container = null;
let root = null;
jest.mock("react-redux");


beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

test("Button Component test", () => {

    
    jest.spyOn(reduxHooks, 'useSelector').mockReturnValue([]);
    jest.spyOn(reduxHooks, 'useDispatch').mockReturnValue([]);
            

    act(() => {root.render( 
        <CreateModal />  
            );

    const elementAdd = screen.findByRole('btnAdd');
    const elementCancel = screen.findByRole('btnCancel');    

    expect(elementAdd).not.toBeUndefined();
    expect(elementCancel).not.toBeUndefined();


    expect(elementAdd).toBeTruthy();
    expect(elementCancel).toBeTruthy();

    elementAdd.then(elementAdd=>expect(elementAdd.textContent).toBe(`Add`));
    elementCancel.then(elementCancel=>expect(elementCancel.textContent).toBe(`Cancel`))    

    
})
});