import "bulma/css/bulma.css";
import "./components/cartCard.scss"
import Router from "./components/routers/Router";
import Store from "./components/store/index";
import {Provider} from "./components/Context"

const App =()=> {

    return (
        <Provider>
            <Store>
                <Router/>
            </Store>
        </Provider>
        
        
    )

};

export default App
