import {Provider} from "react-redux";
import {configureStore} from "@reduxjs/toolkit";
import { combineReducers } from 'redux';
import thunk from "redux-thunk";
import productSlice from "./reducers/productSlice";
import favoriteSlice from "./reducers/favoriteSlice";
import cartSlice from "./reducers/cartSlice";
import modalSlice from "./reducers/modalSlice";



const allReducers = combineReducers({
    productSlice, favoriteSlice, cartSlice, modalSlice
});



const store = configureStore({
    reducer: allReducers,
    middleware: [thunk],
});


export default function Store(props) {
  
   
    return (
        <Provider store={store}>            
                {props.children}        
        </Provider>
    )
}