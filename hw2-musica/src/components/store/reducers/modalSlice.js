import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    isOpen: false,
    id: "",
};

const modalSlice = createSlice({

    name: 'modalSlice',
    initialState,
    reducers: {
        openModal(state, action) {
                    
            state.isOpen= true
            state.id = action.payload
            
            
        },
        closeModal(state, action) {
            state.isOpen= false;
        },       
       
        }    
   
});

export default modalSlice.reducer
export const {openModal, closeModal} = modalSlice.actions