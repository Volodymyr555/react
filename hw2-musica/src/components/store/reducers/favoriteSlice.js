import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    productsFavorite: JSON.parse(window.localStorage.getItem(`favorite`)) || [],
    
};

const favoriteSlice = createSlice({
    name: 'favoriteSlice',
    initialState,
    reducers: {
        changeFavorite(state, {payload}) {
            const {productsFavorite} = state;

            const findedProduct = productsFavorite.find((el)=>{
                return el === payload
                })
                let newAray;
                
                if (findedProduct) {
                    newAray =
                productsFavorite.filter((productId)=>{
                    return productId !== payload
                
                })
                
                }
                
                else{
                    newAray = 
                    [...productsFavorite, payload]
                }
                
                state.productsFavorite = newAray   
                localStorage.setItem(`favorite`, JSON.stringify(newAray))

        },
        deleteFavorite(state, {payload}) {
            const {productsFavorite} = state;
            const newAray = productsFavorite.filter(el => el !== payload)

            state.productsFavorite = newAray  
            localStorage.setItem(`favorite`, JSON.stringify(newAray))

        },
       
        }    
   
});

export default favoriteSlice.reducer
export const {changeFavorite, deleteFavorite} = favoriteSlice.actions