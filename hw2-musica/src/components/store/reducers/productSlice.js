import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

export const initialState = {
    products: [],
    status: '',
};

// 


export const fetchAsync = createAsyncThunk(
    'productSlice/fetchAsync',
    async (_, {rejectWithValue}) => {
        try {
            const response = await fetch('/db.json');
            return await response.json();
        } catch (e) {
            return rejectWithValue(e.message)
        }
    }
)

const productSlice = createSlice({
    name: 'productSlice',
    initialState,   
    extraReducers: {
        [fetchAsync.pending]: (state) => {
            state.status = 'loading';
        },
        [fetchAsync.fulfilled]: (state, action) => {
            state.status = 'loaded';
            state.products = action.payload;
        },
        [fetchAsync.rejected]: (state, action) => {
            state.status = 'rejected: error ' + action.payload;
            state.products = [];
        }

    }
});


export const { } = productSlice.actions;
export default productSlice.reducer