import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    productsCart: JSON.parse(window.localStorage.getItem(`cart`)) || [],
    counter: 1
    
    
};

const cartSlice = createSlice({
    name: 'cartSlice',
    initialState,
    reducers: {
        changeCart(state, {payload}) {
            const {productsCart} = state;
            const findedProduct = productsCart.find((el)=>{
                return el.id === payload
                })
                let newAray=[]
                newAray.push('{id: payload, counter: "1"}')
           
                if (findedProduct) 
                {
                    newAray =  productsCart
                }
                else
                {
                    newAray = 
                    [...productsCart, {id: payload, counter: 1}]
                }
                state.productsCart = newAray
                localStorage.setItem(`cart`, JSON.stringify(newAray))

        },       
        deleteCart(state, {payload}) {
            const {productsCart} = state;                    
            const newAray = productsCart.filter(el => el.id !== payload)
            state.productsCart = newAray  
            localStorage.setItem(`cart`, JSON.stringify(newAray))

        },
        increase(state, action) {
            
            
            const {productsCart} = state;
            
            const newAray = productsCart.map((product) => {
                if (product.id === action.payload) {
                    return  {...product, counter: product.counter + 1};
                    
                }
                return product                
            }
            
            )
            state.productsCart = newAray  
            localStorage.setItem(`cart`, JSON.stringify(newAray))

        },
        decrease(state, action) {
            
            
            const {productsCart} = state;
            
            const newAray = productsCart.map((product) => {
                if (product.id === action.payload) {
                    return  {...product, counter: product.counter - 1};
                    
                }
                return product                
            }
            
            )
            state.productsCart = newAray  
            localStorage.setItem(`cart`, JSON.stringify(newAray))

        }
       
        }
});

export default cartSlice.reducer
export const {changeCart, deleteCart, increase, decrease} = cartSlice.actions