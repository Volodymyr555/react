
import { ReactComponent as Logo } from "../svg/heart-1.svg"
import PropTypes from 'prop-types';
import { changeFavorite } from "./store/reducers/favoriteSlice";
import "./profileCard.scss"
import {useDispatch } from "react-redux";
import { openModal } from "./store/reducers/modalSlice";


function ProfileCard({image, title, price, article, color, id, isFavorite}) {

const dispatch = useDispatch();


const startColor = isFavorite ? "heart" : "";

const handleClickModal =()=>{
    dispatch(openModal(id))  
  
};

const handleClickLogo = (ev)=>{   
    dispatch(changeFavorite(id))    
}
            
    return <div id={id} className="column is-3">
                <div className="card">
        <div  id={id} className="card-image pt-3 pr-3 is-flex is-justify-content-space-between">            
            
            <figure className="image is 1by1">                
                <img className="is-32x32" src={image} style={{width: 300, height: 300}} alt="rose" />
            </figure>
            <figure id={id} className="">
                <Logo id={id} onClick={handleClickLogo} className={startColor} height="30" width="30"/>
            </figure>
        </div>
        <div className="card-content">
            <div className="media-content ">                
                <p className="title is-4">{title}</p>
                <p className="subtitle is-5 mt-3">{price + " $"}</p>
                <p className="subtitle is-6 article">{"art: "+article}</p>
                <p className="subtitle is-6">{"color: "+color}</p>                
            </div>        
            
            <div className="is-flex is-flex-direction-column">             
            </div>
            <button role="btnProfile" id={id} onClick={handleClickModal} className="button is-primary mt-6">Add to cart</button>
        </div>
                    
        
    </div>    
    
    
</div>          
     
    
}

// ProfileCard.propTypes = {
//     title: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired,
//     article: PropTypes.number.isRequired,
//     color: PropTypes.string.isRequired
// };

// ProfileCard.defaultProps = {
//     isFavorite: PropTypes.bool
// }
export default ProfileCard