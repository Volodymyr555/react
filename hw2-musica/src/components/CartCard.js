import "../components/profileCard.scss"
import { deleteCart, increase, decrease } from "./store/reducers/cartSlice";
import { useDispatch } from "react-redux";
import "./cartCard.scss"

function CartCard({image, title, price, article, color, id, counter}) {

    const dispatch= useDispatch();   

const handleClick =()=>{
    
   dispatch(deleteCart(id))
   
}

const handleClickCrease =(ev)=>{
    dispatch(increase(ev.target.id))   

} 

const handleClickDecrease =(ev)=>{
    dispatch(decrease(ev.target.id))   

} 

            
    return <div id={id} className="card column is-7 mt-5" >
                <div className="columns hero is-primary is-flex-direction-row">
                    <div className="column "></div>
                    <h2 className="column has-text-centered">Title:</h2>
                    <h2 className="column has-text-centered">Price:</h2>
                    <h2 className="column has-text-centered">Article:</h2>
                    <h2 className="column has-text-centered">Color:</h2>
                    <h2 className="column has-text-centered">Quantity:</h2>
                    <div className="column has-text-centered"></div>
                </div>
                <div className="columns is-flex is-align-items-center is-flex-direction-row">
                    <figure className="column image has-text-centered">                
                        <img className="" src={image}  alt="rose" />
                    </figure>
                    <p className="column has-text-centered has-text-weight-bold is-size-3">{title}</p>
                    <p className="column has-text-centered">{price + " $"}</p>
                    <p className="column has-text-centered">{"art:"+article}</p>
                    <p className="column has-text-centered">{"color: "+color}</p>
                    <div id='Crease' className="has-text-centered">
                        <button role="Decrease" className="has-text-centered" id={id} onClick={handleClickDecrease} type="button">-</button>
                        <input className="has-text-centered" id={id} type="number" min={1} max={100} onChange={()=>{}} value={counter}/>
                        <button role="Crease" className="has-text-centered"  onClick={handleClickCrease}>+</button>
                    </div>
                    <div className="column has-text-centered">
                        <button role="DeleteCart" id={id} onClick={handleClick} className="delete is-large"></button>
                    </div>                      
                </div>     
          
            </div>          
     
    
}

export default CartCard