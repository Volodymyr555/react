// import { useCallback } from "react";
import { createContext, useState,} from "react";


const ProductsContext = createContext();

function Provider ({children}) {

    
    // const [products, setProducts] = useState([]);
    const [productsCart, setProductsCart] = useState(JSON.parse(window.localStorage.getItem(`cart`)) || []);
    const [productsFavorite, setProductsFavorite] = useState(JSON.parse(window.localStorage.getItem(`favorite`)) || []);
    const [isTable, setIsTable] = useState(false);

    

    

// const fetchProducts = useCallback(async ()=> {
//     return await fetch("/db.json")
//     .then((response) => {
//         return response.json()
//     })
//     .then((data) => {       
//          return setProducts(data);   
//     })

//       }, []);



const changeCart = (id)=>{
const findedProduct = productsCart.find((el)=>{
return el === id
})
let newAray;

if (findedProduct) 
{
    newAray =  productsCart
}
else
{
    newAray = 
    [...productsCart, id]
}
setProductsCart(newAray)   
localStorage.setItem(`cart`, JSON.stringify(newAray))
      }

const deleteCart = (id)=>{
const newAray = productsCart.filter(el => el !== id)

setProductsCart(newAray)   
localStorage.setItem(`cart`, JSON.stringify(newAray))
      }


 const changeFavorite = (id)=>{
const findedProduct = productsFavorite.find((el)=>{
return el === id
})
let newAray;

if (findedProduct) {
    newAray =
productsFavorite.filter((productId)=>{
    return productId !== id

})

}

else{
    newAray = 
    [...productsFavorite, id]
}
setProductsFavorite(newAray)   
localStorage.setItem(`favorite`, JSON.stringify(newAray))
};
const changeTable = ()=>{
    if (isTable) {
        setIsTable(false)
    }else{
        setIsTable(true)
    }
}





    const valueToShare = {   

    productsCart,
    productsFavorite,
    changeFavorite,
    changeCart,
    deleteCart,
    changeTable,
    isTable   

}

 return (
 <ProductsContext.Provider value={valueToShare}>
    {children}
    </ProductsContext.Provider>
    );   
}
export {Provider}
export default ProductsContext