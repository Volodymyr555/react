import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import { deleteCart } from './store/reducers/cartSlice';
import {Formik, Form, Field, ErrorMessage} from "formik";
import * as Yup from "yup";
import TextError from './TextError';


const initialValues = {
    name: "",
    surname: "",
    email: "",    
    address: "",   
    phoneNumber: ""
};



const validationSchema = Yup.object({
    name: Yup.string().required('Required!'),
    surname: Yup.string().required('Required!'),
    email: Yup.string().email('invalid email format').required('Required!'),
    address: Yup.string().required('Required!'),
    phoneNumber: Yup.number().required('Required!'),    
});

function CartForm(props) {
    const products = useSelector(state => state.productSlice.products);
    const productsCart = useSelector(state => state.cartSlice.productsCart);
  
    const order = [];

    products.map(product=>{
        const searchProduct = productsCart.find(el => el.id === product.id);
        if(searchProduct){
                    return   order.push({...product,counter:searchProduct.counter});
                    
              }else{
                return product
              }
    });


    const dispatch= useDispatch();

 const onSubmit = (values) => {    

    productsCart.map(product => dispatch(deleteCart(product.id)))   
    
    
    console.log(order);
    console.log(values);

}; 
   
    return (
        <Formik  initialValues={initialValues} 
        validationSchema={validationSchema} 
        onSubmit={onSubmit}>
            <Form className='is-size-4 box'>
                
                <div className='field'>
                <label htmlFor='name'>Name:</label>
                <Field className='input is-primary' 
                type='text'
                 id="name"
                 name="name"                
                />                
                <ErrorMessage className='is-danger' name='name' component={TextError}/>
                </div>

                <div className='field'>
                <label htmlFor='name'>Surname:</label>
                <Field className='input is-primary' 
                type='text'
                 id="surname"
                 name="surname"                
                />                
                <ErrorMessage name='surname' component={TextError}/>
                </div>

                <div className='field'> 
                <label htmlFor='email'>E-mail:</label>
                <Field className='input is-primary'
                type='email' 
                id="email" 
                name="email"                                
                />
                <ErrorMessage name='email'>
                    {(errorMsg) => 
                     <div className='errors'>{errorMsg}</div>   
                    }
                </ErrorMessage>
                </div>                
                
                <div className='field'>
                <label htmlFor='address'>Address:</label>
                <Field className='input is-primary' 
                name="address"/>
                <ErrorMessage name='address'>
                    {(errorMsg) => 
                     <div className='errors'>{errorMsg}</div>   
                    }
                </ErrorMessage>
                
                </div>                
                <div className='field'>
                    <label htmlFor='primaryPhone'>Phone Number:</label>
                    <Field className='input is-primary' type="text" id="primaryPhone" name="phoneNumber"/>
                    <ErrorMessage name='phoneNumber'>
                    {(errorMsg) => 
                     <div className='errors'>{errorMsg}</div>   
                    }
                </ErrorMessage>
                </div>                

                <button className='button is-primary is-medium' type='submit'>Сheckout</button>
                </Form>
            </Formik>
            
       
    );
}

export default CartForm;