import FavoritesCard from "./FavoritesCard";
import {useSelector} from "react-redux";
import CreateModal from "./CreateModal";


function ShowFavoritesCards () {
    const { isOpen } = useSelector((store) => store.modalSlice)       
    const products = useSelector(state => state.productSlice.products);
    const productsFavorite = useSelector(state => state.favoriteSlice.productsFavorite);

    const newProducts = products.filter((product)=>{
    return productsFavorite.includes(product.id)

})
 const ShowFavorites = newProducts.map(element => {
    
        
        return <FavoritesCard image={element.url} title={element.name} price={element.price} 
        article={element.article} color={element.color} id={element.id} key={element.id} />
        
    })  

    return <section className="mt-5 px-6">
        <h1 className="title has-text-centered has-text-weight-bold mt-6">Favorite</h1>
        <div className="columns is-flex-wrap-wrap">
        
        {ShowFavorites}
        
    </div>
    {isOpen && <CreateModal/>} 
    </section>
    
    
}

export default ShowFavoritesCards