
import ProfileCard from "./ProfileCard";
import ProfileCardTable from "./ProfileCardTable";
import CreateModal from "./CreateModal";
import { useSelector} from "react-redux";
import { useContext } from "react";
import ProductsContext from "./Context"

function ShowListCards () {

    const { isOpen } = useSelector((store) => store.modalSlice)    
    const products = useSelector(state => state.productSlice.products);
    const productsFavorite = useSelector(state => state.favoriteSlice.productsFavorite);
    const {changeTable, isTable} = useContext(ProductsContext)

   
    const handeleChange =()=>{        
        changeTable()
    }


    const showProfileCardTable = products && products.map(element => {    
        
        return <ProfileCardTable image={element.url} title={element.name} price={element.price} 
        article={element.article} color={element.color} id={element.id} key={element.id}isFavorite={productsFavorite.includes(element.id)}      
          />
          
    });

    const showProfileCard = products && products.map(element => {    
        
        return <ProfileCard image={element.url} title={element.name} price={element.price} 
        article={element.article} color={element.color} id={element.id} key={element.id}isFavorite={productsFavorite.includes(element.id)}      
          /> 
        
    });

return <>
        <label className="checkbox m-5">
            <input className="mr-2" onClick={handeleChange} type="checkbox"/>
                            Table
        </label>
        
            {isTable ? 
            (<table className="table is-hoverable is-fullwidth ml-3 mr-3"><tbody>{showProfileCardTable}</tbody></table>) :
            (<div data-tetid="products-item" className="columns is-flex-wrap-wrap mt-5 px-6">{showProfileCard}</div>)}        
        
        {isOpen && <CreateModal/>}
        </>

}

export default ShowListCards