import { deleteFavorite } from "./store/reducers/favoriteSlice";
import { useDispatch } from "react-redux";
import { openModal } from "./store/reducers/modalSlice";
import "./profileCard.scss"


function FavoritesCard({image, title, price, article, color, id}) {
    const dispatsh = useDispatch() 
   
const handleClickModal =()=>{
    dispatsh(openModal(id))    
   
};
const handleClick =()=>{
dispatsh(deleteFavorite(id))
}

            
    return <div id={id} className="column is-3">
                <div className="card">
        <div className="card-image pt-3 pr-3 is-flex is-justify-content-space-between">          
            
            <figure className="image is 1by1">                
                <img className="is-32x32" src={image} style={{width: 300, height: 300}} alt="rose" />
            </figure>
            <button id={id} onClick={handleClick} className="delete is-large"></button>
            
        </div>
        <div className="card-content">
            <div className="media-content ">                
                <p className="title is-4">{title}</p>
                <p className="subtitle is-5 mt-3">{price + " $"}</p>
                <p className="subtitle is-6 article">{"art:"+article}</p>
                <p className="subtitle is-6">{"color: "+color}</p>                
            </div>
        <button role='btnFavorite' id={id} onClick={handleClickModal} className="button is-primary mt-4">Add to cart</button>        
        </div>        
        
    </div>       
    
            </div>           
     
    
}

export default FavoritesCard