import CartCard from "./CartCard";
import {useSelector} from "react-redux";
import CartForm from "./CartForm"; 

function ShowListCartCards () { 
    
    const products = useSelector(state => state.productSlice.products);
    const productsCart = useSelector(state => state.cartSlice.productsCart);

    
    const newProductsCart = [];
    productsCart.map(el=>
        newProductsCart.push(el.id))
    
    let newProducts = products.filter((product)=>{
        return newProductsCart.includes(product.id)
    
    });

    newProducts = newProducts.map((product)=>{
        const searchProduct = productsCart.find(el => el.id === product.id);
        if(searchProduct && searchProduct.counter !== product.counter){
                    return   {...product,counter:searchProduct.counter}
              }else{
                return product
              }
    })


 const ShowProdacts =  newProducts.map(element => {
   
        
        return <CartCard image={element.url} title={element.name} price={element.price} 
        article={element.article} color={element.color} id={element.id} key={element.id} counter={element.counter}/>
        
    })  

    return <div>
                    
                <section className="mt-5 px-6">
                    <h1 className="title has-text-centered has-text-weight-bold mt-6">Shopping cart</h1>
                    <div className="is-flex is-flex-direction-column">                        
                        <div className="columns is-flex is-flex-wrap-wrap is-flex-direction-column is-align-items-center">            
                        {ShowProdacts}        
                        </div>
                        <div className="columns is-flex is-flex-wrap-wrap is-flex-direction-column is-align-items-center mt-5 mb-6">            
                        <CartForm/>        
                        </div>
                        
                    </div>                     
                </section>
            </div>
    
}

export default ShowListCartCards