import {BrowserRouter, useRoutes} from "react-router-dom";
import ShowListCards from "../ShowProfileCards";
import ShowListCartCards from "../ShowCartCards";
import ShowFavoritesCards from "../ShowFavoritesCards";
import Layouts from "../Lauouts/Layouts";
import { useEffect } from "react";
import { fetchAsync } from "../store/reducers/productSlice";
import { useDispatch } from "react-redux";


function Routes () {
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(fetchAsync());
  }, []);  
    return useRoutes ([
        {
            path: '/',
            element: <Layouts />,
            children: [
                {
                    path: '/',
                    element: <ShowListCards/>,
                },
                {
                    path: '/Cart',
                    element: <ShowListCartCards />
                },
                {
                    path: '/Favorites',
                    element: <ShowFavoritesCards />
                }
                
            ]
        }
    ])
}

export default function Router () {
    return (
        <>
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        </>
    )
}