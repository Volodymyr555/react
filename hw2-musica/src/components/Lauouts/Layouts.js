import { Outlet, NavLink } from "react-router-dom";
import basket from "../../svg/basket.svg";
import home from "../../svg/home.svg"
import heart from "../../svg/heart.svg";
import { useSelector } from "react-redux";
import "../profileCard.scss";


export default function Loyout () {
    const productsFavorite = useSelector(state => state.favoriteSlice.productsFavorite);
    const productsCart = useSelector(state => state.cartSlice.productsCart)

   
    return (
        <>
        <div>
        <section className="hero is-primary">
            <div className="hero-body is-flex is-justify-content-space-between is-align-items-center">
                <p className="title is-flex is-align-items-center">... more than Flowers store</p>
                
                <div className="is-flex is-align-items-center">
                    <NavLink to="/"><img className="mr-6" src={home} alt="home" width={50}/></NavLink>
                    <div className="is-flex-wrap-wrap">
                        <div className="is-flex is-justify-content-center mb-5">
                            <NavLink to="/Favorites"><img className="is-flex" src={heart} alt="heart" width={30}/></NavLink>
                            
                            <div className="is-rounded">{productsFavorite.length}</div>
                        </div>               
                        <div className="is-flex is-justify-content-center mt-5">                        
                            <NavLink to="/Cart"><img className="is-flex" src={basket} alt="busket" width={40}/></NavLink>                                                                                     
                            <div className="is-rounded">{productsCart.length}</div>
                        </div>
                    </div>
                    
                </div>               
                
            </div>
        </section>    
       
        
    </div>
    <Outlet></Outlet>
        </>
    )
    
}