import React from "react";
import { changeCart } from "./store/reducers/cartSlice";
import {useSelector, useDispatch } from "react-redux";
import { closeModal } from "./store/reducers/modalSlice";

const CreateModal = ()=>{
  const { id } = useSelector((store) => store.modalSlice)
 
  const dispatch = useDispatch();
 
    const removeClickCard =()=>{ 
      
         dispatch(closeModal())            
    }

    const handleSubmit = () => {

      dispatch(closeModal())    
     
      dispatch(changeCart(id));      
      
          
        };

    
    return  <div onClick={()=>{dispatch(closeModal())}} className="modal is-active">      
    <div onClick={(e)=>{e.stopPropagation()}} className="card">
      <header className="modal-card-head">
        <p className="modal-card-title">Add item to cart?</p>      
      </header>  
      <footer className="modal-card-foot is-justify-content-center">
        <button role="btnAdd"  onClick={handleSubmit}   className="button is-success">Add</button>
        <button role="btnCancel" onClick={removeClickCard} className="button">Cancel</button>
      </footer>
    </div>
  </div>

}

export default CreateModal

