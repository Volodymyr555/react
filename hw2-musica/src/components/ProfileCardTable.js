
import { ReactComponent as Logo } from "../svg/heart-1.svg"
import PropTypes from 'prop-types';
import { changeFavorite } from "./store/reducers/favoriteSlice";
import "./profileCard.scss"
import {useDispatch } from "react-redux";
import { openModal } from "./store/reducers/modalSlice";


function ProfileCardTable({image, title, price, article, color, id, isFavorite}) {

const dispatch = useDispatch();


const startColor = isFavorite ? "heart" : "";

const handleClickModal =()=>{
    dispatch(openModal(id))  
  
};

const handleClickLogo = (ev)=>{   
    dispatch(changeFavorite(id))    
}


return  (
<tr className="">
    <th className="pt-6">
        {id}
    </th>
    <td className="is-flex is-flex-diraction-row">
        <figure className="image is 1by1">                
            <img className="" src={image} style={{width: 100, height: 100}} alt="rose" />
        </figure>
        <figure id={id} className="">
            <Logo id={id} onClick={handleClickLogo} className={startColor} height="30" width="30"/>
        </figure>
    </td>
    <th className="pt-6">{title}</th>
    <td className="pt-6">{price + " $"}</td>
    <td className="pt-6">{"art: "+article}</td>
    <td className="pt-6">{"color: "+color}</td>
    <td className="pr-5"><button id={id} onClick={handleClickModal} className="button is-primary mt-6">Add to cart</button></td>
</tr>)



            


     
    
}

ProfileCardTable.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    article: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired
};

ProfileCardTable.defaultProps = {
    isFavorite: PropTypes.bool
}
export default ProfileCardTable