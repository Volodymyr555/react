
import React from "react";
import "./style.scss";
import Actions from "./Actions";




export function ShowModalFirst ({header, text, closeButton}) {      
    return <div className="modal-1_container">
        <div className="modal-1_header">
            <h3>{header}</h3>
            <div className="cross">×</div>
        </div>
        <div>
            <p>{text}</p>
            {closeButton}
            <div>
                <Actions />
            </div>
        </div>
    </div>
     
       
}


export function ShowModalSecond ({header, text, closeButton}) {

    return <div className="modal-2_container">
    <div className="modal-2_header">
        <h3>{header}</h3>
        <div className="cross">×</div>
    </div>
    <div>
        <p>{text}</p>
        {closeButton}
        <div>
            <Actions />
        </div>
    </div>
</div>
    
}



