import {ButtonFirst, ButtonSecond} from "./Buttons";
import {ShowModalFirst, ShowModalSecond} from "./Modal";
import "./style.scss"


function App() {

const handleClick = (event)=>{
  const divModal1 = document.querySelector(".modal-1");
    const divModal2 = document.querySelector(".modal-2");
    const btn1 = document.querySelector(".btn-1");
    const btn2 = document.querySelector(".btn-2");
    const container = document.querySelector(".container");
    
    
    if ((event.target !== divModal1) && (event.target !== divModal2) && (event.target !== btn1) &&  (event.target !== btn2)) {
            divModal1.classList.remove("active");
            divModal2.classList.remove("active");
            container.classList.remove("shadow")
        }
        }


return  <div onClick={handleClick} className="container">
              <div className="buttons">
                <ButtonFirst Color={"red"} text={"Open first modal"}  />
                <ButtonSecond Color={"blue"} text={"Open second modal"}  />              
              </div>
              <div className="modal-1">
                <ShowModalFirst header={"Do you want delete this file?"} text={"Once you delete this file, it won't be possible to undo this action. Are you sure you whant to delete it?"} closeButton={false} />
              </div>
              <div className="modal-2">
                <ShowModalSecond header={"Do you want to open this file?"} text={"After you open the file, you need to get additional data. Are you sure you want to open the file??"} />
              </div>
            </div>        
    
}

export default App